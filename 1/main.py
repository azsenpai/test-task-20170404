#!/usr/bin/python

def solution(A):
    res = 0

    n = len(A)
    i = 0

    while i < n:
        j = i + 1

        while j < n and A[i] == A[j]:
            j += 1

        if ((i-1 < 0 or A[i-1] < A[i]) and (j >= n or A[j-1] > A[j])) \
            or ((i-1 < 0 or A[i-1] > A[i]) and (j >= n or A[j-1] < A[j])) \
        :
            res += 1

        i = j

    return res


if __name__ == '__main__':
    A = [2, 2, 3, 4, 3, 3, 2, 2, 1, 1, 2, 5]
    print(solution(A))
