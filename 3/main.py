#!/usr/bin/python

def solution(S):
    d = {}
    free_phone = None

    for s in S.split(chr(10)):
        time, phone = s.split(',')

        h, m, s = map(int, time.split(':'))
        total = h * 3600 + m * 60 + s

        prev = d.get(phone, [0, 0])

        if total >= 300:
            d[phone] = [prev[0] + total, prev[1] + (total + 59) // 60 * 150]
        else:
            d[phone] = [prev[0] + total, prev[1] + total * 3]

        if free_phone == None \
            or (d[free_phone][0] < d[phone][0]) \
            or (d[free_phone][0] == d[phone][0] and free_phone > phone) \
        :
            free_phone = phone

    # print(d)

    res = 0

    for phone in d:
        if phone != free_phone: res += d[phone][1]

    return res


if __name__ == '__main__':
    S = "00:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090"
    print(solution(S))
